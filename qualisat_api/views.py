from django.shortcuts import render, redirect
from .forms import SignUpForm
from django.contrib import messages
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth import authenticate, login, logout

# Create your views here.

def index(response):
    if response.method == 'POST':
        form_sign_in = AuthenticationForm(data=response.POST)
        form_sign_up = SignUpForm(response.POST)
        if response.POST.get('submit') == 'sign_in':
            if form_sign_in.is_valid():
                username = form_sign_in.cleaned_data.get('username')
                password = form_sign_in.cleaned_data.get('password')
                user = authenticate(username=username, password=password)
                if user is not None:
                    login(response, user)
                    #messages.success(response, f"You are now logged in as {username}")
                    return redirect("/")
                else:
                    messages.error(response, "Invalid username or password.")
            else:
                  messages.error(response, "Invalid username or password2.")
         
        elif response.POST.get('submit') == 'sign_up':
            if form_sign_up.is_valid():
                form_sign_up.save()
                messages.success(response, f"You are registred with success.")
            else:
                messages.error(response, form_sign_up.errors)

        elif response.POST.get('submit') == 'logout':
            logout(response)
        
    else:
        form_sign_up = SignUpForm()
        form_sign_in = AuthenticationForm()
        
    return render(response, "qualisat_api/index.html", {"form_sign_up": form_sign_up, "form_sign_in": form_sign_in})